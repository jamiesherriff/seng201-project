package tamagotchi;

import static org.junit.Assert.*;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SetupGuiTest {
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCheckPlayerNames() {
		 SetupGui gui =  new SetupGui();
		assertFalse (gui.checkPlayerNames( "jamie",  "BOB",  "PLAYER123"));
		assertTrue (gui.checkPlayerNames( "jamie",  "jamie",  "PLAYER123"));
		assertTrue (gui.checkPlayerNames( "BOB",  "jamie",  "jamie"));
		assertTrue (gui.checkPlayerNames( "jamie",  "BOB",  "jamie"));

		
	}

	@Test
	public void testCheckPetNames() {
		JTextField textField1 = new JTextField();
		textField1.setText("Pet 1");
		JTextField textField2 = new JTextField();
		textField2.setText("Pet 2");
		JTextField textField3 = new JTextField();
		textField3.setText("Pet 3");
		JTextField textField4 = new JTextField();
		textField4.setText("Pet 1");
		JTextField textField5 = new JTextField();
		textField5.setText("Pet 2");
		JTextField textField6 = new JTextField();
		textField6.setText("Pet 3");
		JTextField textField7 = new JTextField();
		textField7.setText("Pet 1");
		JTextField textField8 = new JTextField();
		textField8.setText("Pet 2");
		JTextField textField9 = new JTextField();
		textField9.setText("Pet 3");


		
		SetupGui gui1 =  new SetupGui();
		assertTrue (gui1.checkPetNames(textField1, textField1, textField1, textField1, textField1, 
				textField1, textField1, textField1, textField1));
		assertFalse (gui1.checkPetNames(textField1, textField2, textField3, textField4, textField5, 
				textField6, textField7, textField8, textField9));
		assertTrue (gui1.checkPetNames(textField9, textField2, textField3, textField9, textField5, 
				textField6, textField7, textField8, textField9));
		assertTrue (gui1.checkPetNames(textField9, textField2, textField3, textField9, textField5, 
				textField6, textField7, textField8, textField9));
		

		
	}

}
