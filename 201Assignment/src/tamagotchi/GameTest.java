package tamagotchi;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GameTest {



	@Test
	public void testUpdateWeather() {
		Game newgame = new Game();
		String weather = newgame.updateWeather(); 
		assertTrue(weather.equals("Sunny") || weather.equals("Cloudy") || weather.equals("Raining") );
		
	}


}
