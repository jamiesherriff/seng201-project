/**Jamie's Tamagotchi Game
 * @Version 1.0
 * @author Jamie Sherriff

 */

package tamagotchi;
import java.awt.EventQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JDialog;


	/**
	 * High level Game class that contains methods and parameters
	 * required to run the game
	 * @author Jamie
	 * 
	 */
public class  Game {
		/**
		 * Number of players playing the game
		 */
	static int numPlayers;
		/**
		 * Number of days to play
		 */
	static int daysToPlay =5;	
		/**
		 * player 1 Name
		 */
	static String player1Name ="";
		/**
		 * Player 2 Name
		 */
	static String player2Name = "";
		/**
		 * Player 3 Name
		 */
	static String player3Name = "";	
		/**
		 * Player 1 Pet Number
		 */
	static int player1PetNum;
		/**
		 * Player 2 Pet number
		 */
	static int player2PetNum;
		/**
		 * Player 3 Pet Number
		 */
	static int player3PetNum;
		/**
		 * Array list that hold's player 1 pet names
		 */
	static List<String> petNameList1 = new ArrayList<String>();
		/**
		 * Array list that hold's player 1 pet species
		 */
	static List<String> speciesList1 = new ArrayList<String>();
		/**
		 * Array list that hold's player 2 pet names
		 */
	static List<String> petNameList2 = new ArrayList<String>();
		/**
		 * Array list that hold's player 2 pet species
		 */
	static List<String> speciesList2 = new ArrayList<String>();
		/**
		 * Array list that hold's player 3 pet names
		 */
	static List<String> petNameList3 = new ArrayList<String>();
		/**
		 * Array list that hold's player 3 pet species
		 */
	static List<String> speciesList3 = new ArrayList<String>();
		/**
		 * Master list that holds all the games pets
		 */
	static List<int[]> petList = new ArrayList<int[]>();
		/**
		 * Keeps track of the day count
		 */
	static int playerActions=2;
		/**
		 * Keeps track of the day count
		 */
	static int daycount=0;
		/**
		 *  boolean condition if end turn is met
		 */
	static boolean endTurn = false;
		/**
		 * boolean condition if end day is met
		 */
	static boolean endDay = false;
		/**
		 * boolean condition if the game has started
		 */
	static boolean startGame=false;
		/**
		 * boolean condition if the game gui has been setup
		 */
	static boolean guiSetup = true;
		/**
		 *  Variable that stores the current weather
		 */
	static String weather;
		/**
		 * Variable that stores the winning player 
		 */
	static String winningPlayer="";
		/**
		 * Player position that increases with each end day
		 */
	static int playerPostion;
	/**
	 * Runs the start up Gui and catches exceptions.
	 */
	final void gameSetup(){
		//Intro GUI
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IntroGui window = new IntroGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});


	}
	/**
	 * Runs the End/Post Game Gui and catches exceptions
	 * Shows Player scores and the winner
	 */

	final void endGame()	{
		try {
			EndDialog dialog = new EndDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	/**
	 * Build a list of pets for the game
	 * Finds which species a player chose
	 * updates each pet to its species
	 */	
	static void buildPets()	{
		//Player 1
		for (int i = 0; i < 3; i++) {

			if (i >= speciesList1.size()){
				petList.add(Pets.emptyPet);}
			else{
				if (speciesList1.get(i) == "Mametachi"){
					petList.add(new int[]{50, 50, 40, 60, 6, 2, 0, 0, 1, 1, 0});}
				else if (speciesList1.get(i) == "Tamatchi")	{
					petList.add(new int[] {40, 80, 30, 30, 8, 2, 0, 0, 2, 1, 0});}
				else if (speciesList1.get(i) == "Maskutchi")	{
					petList.add(new int[] {50, 40, 70, 50, 4, 2, 0, 0, 3, 1, 0});
				}
			}
		}
		// Player 2
		for (int i = 0; i < 3; i++) {

			if (i >= speciesList2.size()){
				petList.add(Pets.emptyPet);}
			else{
				if (speciesList2.get(i) == "Mametachi"){
					petList.add(new int[]{ 50, 50, 40, 60, 6, 2, 0, 0, 1, 2, 0});
				}
				else if (speciesList2.get(i) == "Tamatchi")	{
					petList.add(new int[] {40, 80, 30, 30, 8, 2, 0, 0, 2, 2, 0});
				}
				else if (speciesList2.get(i) == "Maskutchi")	{
					petList.add(new int[] {50, 40, 70, 50, 4, 2, 0, 0, 3, 2, 0});
				}
			}
		}
		// Player 3
		for (int i = 0; i < 3; i++) {

			if (i >= speciesList3.size()){
				petList.add(Pets.emptyPet);
				}	else	{
				if (speciesList3.get(i) == "Mametachi"){
					petList.add(new int[]{ 50,50,40,60,6,2,0,0,1,3,0});
					}	else if (speciesList3.get(i) == "Tamatchi")	{
					petList.add(new int[] {40,80,30,30,8,2,0,0,2,3,0});
					}	else if (speciesList3.get(i) == "Maskutchi")	{
				petList.add(new int[] {50,40,70,50,4,2,0,0,3,3,0});}
			}
		}

	}
	/**
	 * Makes a pet sick at random
	 */
	final void makePetSick(){	
		Random rand = new Random();
		int num = rand.nextInt(9);
		try {
			petList.get(num)[6]=1;
			System.out.println("Pet "+(num+1) + " is now sick");
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
	}

	/**
	 * Changes the weather
	 * @return string
	 */
	final String updateWeather()	{
		String[] weatherList = {"Sunny", "Cloudy", "Raining"};
		Random rand = new Random();
		int num = rand.nextInt(3);
		try {
			Gui.weatherIcon.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+weatherList[num] +".png")));
			Gui.weatherStatusText.setText("Weather: "+weatherList[num]);
			System.out.println("The Day is now "+ weatherList[num]);
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//Catches Null exception on startup
			//e.printStackTrace();
		}
		return weatherList[num];
	}


	/**
	 * Plays out a GameDay
	 * Waits for user action from the GUI
	 * Increments end turn when end turn has occurred
	 * Increments each day when end day has occurred
	 * Works for any ammount of players
	 */

	final void playGame()	{
		gameSetup();
		//Play game for all Players
		while (startGame == false) {
			startGame = true;
			while (daycount < daysToPlay ){
				Pets.resetPetActions();
				if (daycount != 0){
					Players.calculatescorePlayer1(Players.player1, player1PetNum);
					Players.calculatescorePlayer2(Players.player2, player2PetNum);
					Players.calculatescorePlayer3(Players.player3, player3PetNum);
					weather = updateWeather();
					Pets.weatherPetUpdate(weather);
					Pets.updatePetLimits();
					Pets.endOfDay();
					Health.decreaseHealth();
					makePetSick();
					
				}
				endDay = false;
				playerPostion = 0;
				while (endDay == false && playerPostion <numPlayers)	{
					endTurn = false;
					playerPostion++;
					try {
						Thread.sleep(100);
						if (playerPostion < numPlayers)	{
							Gui.txtPlayersTurn.setText("Player Turn: Player "+playerPostion);
							Gui.btnEndPlayerTurn.setEnabled(true);}
						else{
							Gui.txtPlayersTurn.setText("Player Turn: Player "+playerPostion);
							Gui.btnEndPlayerTurn.setEnabled(false);
							Gui.nextDayButton.setEnabled(true);
						}
					} catch (Exception e1) {
						// Catches premature indexing and thread interruption
					}
					while (playerActions >= 0 && endTurn == false){

						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
						}
					}
				}
			}
			if (daycount != 0){
				Players.calculatescorePlayer1(Players.player1, player1PetNum);
				Players.calculatescorePlayer2(Players.player2, player2PetNum);
				Players.calculatescorePlayer3(Players.player3, player3PetNum);
				winningPlayer = Players.calculateWinner(Players.player1, Players.player2, Players.player3);
				Gui.frame.dispose();
				endGame();
			}
		}
	}
	/**
	 * Run the Tamagotchi Game by calling the playGame function
	 * @param args
	 */
	public static void main(final String[] args) {
		// TODO Auto-generated method stub
		Game newgame = new Game();
		newgame.playGame();



	}
}
