package tamagotchi;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GameTest.class, PetsTest.class, PlayersTest.class,
		SetupGuiTest.class })
public class TamagotchiAllTests {

}
