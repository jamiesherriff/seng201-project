


package tamagotchi;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
	/**
	 * Main Gui window that players play on
	 * @author Jamie
	 *
	 */
public class Gui {

	static int[] currentPet;
	static int [] currentPlayer;
	static String currentPetName;
	static String currentPlayerName;
	//int currentPet;
	static JFrame frame;
	private JTextField txtPlayerPets;
	private JTextField txtPlayerPets_1;
	private JTextField txtPlayerPets_2;
	private JTextField txtPet;
	private JTextField txtDayCount;
	static JTextField txtPlayersTurn;
	static JTextField txtActionsLeft;
	private JTextField player1Name;
	private JTextField player2Name;
	private JTextField player3Name;
	private JTextField txtMoney;
	private JTextField PetActions;
	private JTextField txtScore;
	static JTextField weatherStatusText;
	private JTextField txtDaysPlaying;
	static JLabel weatherIcon;
	private JTextField txtPlayerStats;
	private JTextField txtAge;
	private JTextField dobTxt;
	private TextArea GameDialog;
	static JButton btnEndPlayerTurn;
	static JButton nextDayButton;
	private JTextField txtPetSick;
	private JTextField petSick;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			@SuppressWarnings("static-access")
//			public void run() {
//				try {
//
//					Gui window = new Gui();
//					window.frame.setVisible(true);
//				}
//				catch (Exception e) {
//					System.out.println("ERROR!");
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}


	private void updateTextArea(final String text) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GameDialog.append(text);
			}
		});
	}
	/** Redirects print streams from system to the Game Dialog
	 *Source and credit goes to:
	 *@link http://unserializableone.blogspot.co.nz/2009/01/redirecting-systemout-and-systemerr-to.html     	 
	 */
	private void redirectSystemStreams() {
		OutputStream out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				updateTextArea(String.valueOf((char) b));
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				updateTextArea(new String(b, off, len));
			}

			@Override
			public void write(byte[] b) throws IOException {
				write(b, 0, b.length);
			}
		};

		System.setOut(new PrintStream(out, true));
		System.setErr(new PrintStream(out, true));
	}



	/**
	 * Initialize the contents of the frame.
	 */
	void initialize() {
		redirectSystemStreams();	
		final Game newgame = new Game();
		frame = new JFrame();
		frame.setTitle("Tamagotchi Game");
		frame.getContentPane().setEnabled(false);
		frame.setMinimumSize(new Dimension(818, 760));
		frame.setBounds(500, 300, 818, 760);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("New Game");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("new game is being launched");
				frame.dispose();
				newgame.gameSetup();
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem exitMenuItem = new JMenuItem("Exit");
		exitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mnNewMenu.add(exitMenuItem);

		txtPlayerPets = new JTextField();
		txtPlayerPets.setBounds(392, 35, 85, 28);
		txtPlayerPets.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtPlayerPets.setEditable(false);
		txtPlayerPets.setText("Player1 Pets");
		txtPlayerPets.setColumns(10);

		txtPlayerPets_1 = new JTextField();
		txtPlayerPets_1.setBounds(506, 35, 85, 28);
		txtPlayerPets_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtPlayerPets_1.setEditable(false);
		txtPlayerPets_1.setText("Player2 Pets");
		txtPlayerPets_1.setColumns(10);

		txtPlayerPets_2 = new JTextField();
		txtPlayerPets_2.setBounds(618, 35, 85, 28);
		txtPlayerPets_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtPlayerPets_2.setEditable(false);
		txtPlayerPets_2.setText("Player3 Pets");
		txtPlayerPets_2.setColumns(10);

		//Health Bar
		final JProgressBar healthBar = new JProgressBar();
		healthBar.setFont(new Font("SansSerif", Font.BOLD, 14));
		healthBar.setStringPainted(true);
		healthBar.setForeground(Color.BLUE);
		healthBar.setString("Health");
		healthBar.setBounds(367, 443, 66, 33);
		frame.getContentPane().add(healthBar);

		//Hunger Bar
		final JProgressBar hungerBar = new JProgressBar();
		hungerBar.setFont(new Font("SansSerif", Font.BOLD, 14));
		hungerBar.setForeground(Color.GREEN);
		hungerBar.setStringPainted(true);
		hungerBar.setString("Hunger");
		hungerBar.setBounds(445, 443, 60, 33);
		frame.getContentPane().add(hungerBar);

		//Mood Bar
		final JProgressBar moodBar = new JProgressBar();
		moodBar.setFont(new Font("SansSerif", Font.BOLD, 14));
		moodBar.setForeground(Color.BLUE);

		moodBar.setStringPainted(true);
		moodBar.setBounds(589, 443, 60, 33);
		frame.getContentPane().add(moodBar);
		moodBar.setString("Mood");

		// Weight Bar
		final JProgressBar weightBar = new JProgressBar();
		weightBar.setFont(new Font("SansSerif", Font.BOLD, 14));
		weightBar.setMaximum(10);
		weightBar.setStringPainted(true);
		weightBar.setString("Weight");
		weightBar.setForeground(Color.PINK);
		weightBar.setBounds(661, 443, 60, 33);
		frame.getContentPane().add(weightBar);

		//Energy Bar
		final JProgressBar energyBar = new JProgressBar();
		energyBar.setFont(new Font("SansSerif", Font.BOLD, 14));

		energyBar.setStringPainted(true);
		energyBar.setString("Energy");
		energyBar.setForeground(Color.ORANGE);
		energyBar.setBounds(517, 443, 60, 33);
		frame.getContentPane().add(energyBar);



		//Play with Pet Button Button
		final JButton playButton = new JButton("Interact With Pet");
		playButton.setBounds(28, 47, 151, 60);
		playButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		playButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (currentPet==null){
					String toy = null;

					Pets.interactPet(currentPet, toy,null,null,null);
				}
				else{	
					String interact = Pets.getPlayOptions();
					Pets.interactPet(currentPet,  interact, currentPetName, currentPlayerName,currentPlayer);
					Pets.petJealousy(currentPlayer);
					currentPet=null;
					currentPlayer=null;
					txtPlayerStats.setText("No Pet Selected");
					txtActionsLeft.setText("Actions Left: ");
					txtPet.setText("No Pet selected");
					txtMoney.setText("Money: " );
					PetActions.setText("0 Actions");
					healthBar.setValue(0);
					hungerBar.setValue(0);
					energyBar.setValue(0);
					moodBar.setValue(0);
					weightBar.setValue(0);	
				}


			}
		});

		//Feed Pet button
		final JButton feedButton = new JButton("Feed Pet");
		feedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {		
				if (currentPet==null){
					String food = null;
					Pets.feedpet(currentPet, currentPlayer, food,null,null,null);
				}

				else if(Game.playerActions < 1)
				{
					JOptionPane.showMessageDialog(frame, "You have no actions left please end the turn");
				}	
				else{	
					String food = Pets.getFood();
					Pets.feedpet(currentPet,currentPlayer, 
							food,currentPetName, currentPlayerName,currentPlayer);
					Pets.petJealousy(currentPlayer);
					currentPet=null;
					currentPlayer=null;
					txtPlayerStats.setText("No Pet Selected");
					txtActionsLeft.setText("Actions Left: ");
					txtPet.setText("No Pet selected");
					txtMoney.setText("Money: " );
					PetActions.setText("0 Actions");
					healthBar.setValue(0);
					hungerBar.setValue(0);
					energyBar.setValue(0);
					moodBar.setValue(0);
					weightBar.setValue(0);	
				}


			}
		});
		feedButton.setBounds(28, 120, 151, 60);
		feedButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));

		//Sleep button
		final JButton sleepButton = new JButton("Put Pet to Sleep");
		sleepButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentPet==null){
					Pets.sleepPet(currentPet, currentPlayer,null,null,null);
				}
				else{	
					Pets.sleepPet(currentPet, currentPlayer,currentPetName, currentPlayerName,currentPlayer);
					Pets.petJealousy(currentPlayer);
					txtActionsLeft.setText("Actions Left: "+ Game.playerActions);
					currentPet=null;
					currentPlayer=null;
					txtPlayerStats.setText("No Pet Selected");
					txtActionsLeft.setText("Actions Left: ");
					txtPet.setText("No Pet selected");
					txtMoney.setText("Money: " );
					PetActions.setText("0 Actions");
					healthBar.setValue(0);
					hungerBar.setValue(0);
					energyBar.setValue(0);
					moodBar.setValue(0);
					weightBar.setValue(0);	
				}
			}
		});
		sleepButton.setBounds(28, 193, 151, 60);
		sleepButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));

		// End and next day turn Button
		nextDayButton = new JButton("Procceed to next Day");
		btnEndPlayerTurn = new JButton("End Player Turn");
		btnEndPlayerTurn.setEnabled(false);
		btnEndPlayerTurn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				currentPet=null;
				currentPlayer=null;
				txtPlayerStats.setText("No Pet Selected");
				txtActionsLeft.setText("Actions Left: ");
				txtPet.setText("No Pet selected");
				txtMoney.setText("Money: " );
				txtScore.setText("Score: ");
				PetActions.setText("0 Actions");
				healthBar.setValue(0);
				hungerBar.setValue(0);
				energyBar.setValue(0);
				moodBar.setValue(0);
				weightBar.setValue(0);	
				Game.endTurn= true;
			}

		});
		btnEndPlayerTurn.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnEndPlayerTurn.setBounds(28, 266, 151, 57);
		frame.getContentPane().add(btnEndPlayerTurn);
		//btnEndPlayerTurn.setEnabled(true);	
		if(Game.numPlayers == 1){
			btnEndPlayerTurn.setEnabled(false);	
		}
		else
		{
			btnEndPlayerTurn.setEnabled(true);
		}
		//Next Day Button
		nextDayButton.setBounds(28, 332, 151, 60);
		if (Game.numPlayers > 1){
			nextDayButton.setEnabled(false);
		}
		nextDayButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextDayButton.setEnabled(true);
				Game.daycount++;
				txtPlayerStats.setText("No Pet Selected");
				txtActionsLeft.setText("Actions Left: ");
				txtPet.setText("No Pet selected");
				txtMoney.setText("Money: " );
				PetActions.setText("0 Actions");
				healthBar.setValue(0);
				hungerBar.setValue(0);
				energyBar.setValue(0);
				moodBar.setValue(0);
				weightBar.setValue(0);	
				txtDayCount.setText("Day count: "+Game.daycount);
				txtScore.setText("Score: ");
				dobTxt.setText(""+Game.daycount);
				txtAge.setText("Age");
				dobTxt.setText(""+Game.daycount);
				currentPet=null;
				Game.endTurn = true;
				Game.endDay = true;
				Gui.nextDayButton.setEnabled(false);


			}

		});
		nextDayButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));

		final JButton reviveButton = new JButton("Revive Pet");
		reviveButton.setBounds(28, 405, 151, 60);
		reviveButton.setEnabled(false);
		reviveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pets.revivePet(currentPet,  currentPlayer,    currentPetName,  currentPlayerName,  currentPlayer);
				btnEndPlayerTurn.setEnabled(true);
			}
		});
		reviveButton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));




		//Pet Button 1
		final JButton pet1Button = new JButton("");
		pet1Button.setToolTipText("Pet 1");
		pet1Button.setEnabled(true);
		// Button 1 action
		pet1Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPlayer=Players.player1;
				currentPet=Game.petList.get(0);
				currentPetName = Game.petNameList1.get(0);
				currentPlayerName= Game.player1Name;
				txtPet.setText(Game.petNameList1.get(0)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);

				//Check if pet is dead
				if(Game.petList.get(0)[0]<= 0){
					pet1Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);

				}
				else{
					pet1Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList1.get(0)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);

				}
			}
		});
		pet1Button.setBounds(392, 65, 85, 85);
		if (Game.speciesList1.isEmpty()){
			pet1Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Mametachi.png")));	
		}
		else{
			pet1Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList1.get(0)+".png")));
		}

		//Pet Button 2
		final JButton pet2Button = new JButton("");		
		pet2Button.setToolTipText("Pet 2");
		pet2Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/red_circle1.png")));
		pet2Button.setEnabled(false);
		if (Game.player1PetNum >= 2){
			pet2Button.setEnabled(true);
			pet2Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList1.get(1)+".png")));
		}

		pet2Button.addActionListener(new ActionListener() { //Pet 2 action
			public void actionPerformed(ActionEvent e) {
				currentPet=Game.petList.get(1);
				currentPlayer=Players.player1;
				currentPetName = Game.petNameList1.get(1);
				currentPlayerName= Game.player1Name;
				txtPet.setText(Game.petNameList1.get(1)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);
				//Check if pet is dead
				if(Game.petList.get(1)[0]<= 0){
					pet2Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);


				}
				else{
					pet2Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList1.get(1)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);
				}

			}
		});		
		pet2Button.setBounds(392, 168, 85, 85);

		//Pet Button 3
		final JButton pet3Button = new JButton("");
		pet3Button.setToolTipText("Pet 3");
		pet3Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPet=Game.petList.get(2);
				currentPlayer=Players.player1;
				currentPetName = Game.petNameList1.get(2);
				currentPlayerName= Game.player1Name;
				txtPet.setText(Game.petNameList1.get(2)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);
				//Check if pet is dead
				if(Game.petList.get(2)[0]<= 0){
					pet3Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);


				}
				else{
					pet3Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList1.get(2)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);

				}
			}
		});
		pet3Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/red_circle1.png")));
		pet3Button.setEnabled(false);
		if (Game.player1PetNum >= 3){
			pet3Button.setEnabled(true);
			pet3Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList1.get(2)+".png")));
		}
		pet3Button.setBounds(392, 271, 85, 85);

		//Pet Button 4
		final JButton pet4Button = new JButton("");
		pet4Button.setToolTipText("Pet 4");
		pet4Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPet=Game.petList.get(3);
				currentPlayer=Players.player2;
				currentPetName = Game.petNameList2.get(0);
				currentPlayerName= Game.player2Name;
				txtPet.setText(Game.petNameList2.get(0)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);
				//Check to see if Pet is dead
				if(Game.petList.get(3)[0]<= 0){
					pet4Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);


				}
				else{
					pet4Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList2.get(0)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);
				}
			}
		});
		pet4Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/red_circle1.png")));
		pet4Button.setEnabled(false);
		if (Game.player2PetNum >= 1){
			pet4Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList2.get(0)+".png")));
			pet4Button.setEnabled(true);
		}
		pet4Button.setBounds(506, 65, 85, 85);

		//Pet Button 5
		final JButton pet5Button = new JButton("");
		pet5Button.setToolTipText("Pet 5");
		pet5Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPet=Game.petList.get(4);
				currentPlayer=Players.player2;
				currentPetName = Game.petNameList2.get(1);
				currentPlayerName= Game.player2Name;
				txtPet.setText(Game.petNameList2.get(1)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);
				//Check to see if Pet is dead
				if(Game.petList.get(4)[0]<= 0){
					pet5Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);


				}
				else{
					pet5Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList2.get(1)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);
				}
			}
		});
		pet5Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/red_circle1.png")));
		pet5Button.setEnabled(false);
		if (Game.player2PetNum >= 2){
			pet5Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList2.get(1)+".png")));
			pet5Button.setEnabled(true);		
		}
		pet5Button.setBounds(506, 168, 85, 85);

		//Pet Button 6
		final JButton pet6Button = new JButton("");
		pet6Button.setToolTipText("Pet 6");
		pet6Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPet=Game.petList.get(5);
				currentPlayer=Players.player2;
				currentPetName = Game.petNameList2.get(2);
				currentPlayerName= Game.player2Name;
				txtPet.setText(Game.petNameList2.get(2)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);
				//Check to see if Pet is dead
				if(Game.petList.get(5)[0]<= 0){
					pet6Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);


				}
				else{
					pet6Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList2.get(2)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);
				}
			}
		});
		pet6Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/red_circle1.png")));
		pet6Button.setEnabled(false);
		if (Game.player2PetNum >= 3){
			pet6Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList2.get(2)+".png")));
			pet6Button.setEnabled(true);
		}
		pet6Button.setBounds(506, 271, 85, 85);

		//Pet Button 7
		final JButton pet7Button = new JButton("");
		pet7Button.setToolTipText("Pet 7");
		pet7Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/red_circle1.png")));
		pet7Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent paramActionEvent) {
				currentPet=Game.petList.get(6);
				currentPlayer=Players.player3;
				currentPetName = Game.petNameList3.get(0);
				currentPlayerName= Game.player3Name;
				txtPet.setText(Game.petNameList3.get(0)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);
				//Check to see if Pet is dead
				if(Game.petList.get(6)[0]<= 0){
					pet7Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);


				}
				else{
					pet7Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList3.get(0)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);
				}
			}
		});
		pet7Button.setEnabled(false);
		if (Game.player3PetNum >= 1){
			pet7Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList3.get(0)+".png")));
			pet7Button.setEnabled(true);
		}
		pet7Button.setBounds(618, 65, 85, 85);

		//Pet Button 8
		final JButton pet8Button = new JButton("");
		pet8Button.setToolTipText("Pet 8");
		pet8Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPet=Game.petList.get(7);
				currentPlayer=Players.player3;
				currentPetName = Game.petNameList3.get(1);
				currentPlayerName= Game.player3Name;
				txtPet.setText(Game.petNameList3.get(1)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);
				//Check if Pet is Dead
				if(Game.petList.get(7)[0]<= 0){
					pet8Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);


				}
				else{
					pet8Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList3.get(1)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);
				}
			}
		});
		pet8Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/red_circle1.png")));
		pet8Button.setEnabled(false);
		if (Game.player3PetNum >= 2){
			pet8Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList3.get(1)+".png")));
			pet8Button.setEnabled(true);
		}
		pet8Button.setBounds(618, 168, 85, 85);

		//Pet Button 9
		final JButton pet9Button = new JButton("");
		pet9Button.setToolTipText("Pet 9");
		pet9Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPet=Game.petList.get(8);
				currentPlayer=Players.player3;
				currentPetName = Game.petNameList3.get(2);
				currentPlayerName= Game.player3Name;
				txtPet.setText(Game.petNameList3.get(2)+" Selected");
				healthBar.setValue(currentPet[0]);
				hungerBar.setValue(currentPet[1]);
				energyBar.setValue(currentPet[2]);
				moodBar.setValue(currentPet[3]);
				weightBar.setValue(currentPet[4]);
				PetActions.setText(currentPet[5]+" Actions Left");
				//Update Player statistics
				petSick.setText(Pets.isPetSick(currentPet));
				txtActionsLeft.setText("Actions Left: "+ Players.getPlayerActions(currentPlayer));
				txtPlayerStats.setText("Player Stats: Player "+currentPlayer[2]);
				txtMoney.setText("Money: " +currentPlayer[0]);
				txtScore.setText("Score: "+currentPlayer[1]);
				//Check if pet is dead
				if(Game.petList.get(8)[0]<= 0){
					pet9Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Tombstone.png")));
					playButton.setEnabled(false);
					feedButton.setEnabled(false);
					sleepButton.setEnabled(false);
					nextDayButton.setEnabled(false);
					reviveButton.setEnabled(true);


				}
				else{
					pet9Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList3.get(2)+".png")));
					playButton.setEnabled(true);
					feedButton.setEnabled(true);
					sleepButton.setEnabled(true);
					reviveButton.setEnabled(false);
				}
			}
		});
		pet9Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/red_circle1.png")));
		pet9Button.setEnabled(false);
		if (Game.player3PetNum >= 3){
			pet9Button.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/"+Game.speciesList3.get(2)+".png")));
			pet9Button.setEnabled(true);
		}
		pet9Button.setBounds(618, 271, 85, 85);


		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(reviveButton);
		frame.getContentPane().add(playButton);
		frame.getContentPane().add(feedButton);
		frame.getContentPane().add(sleepButton);
		frame.getContentPane().add(nextDayButton);
		frame.getContentPane().add(pet1Button);
		frame.getContentPane().add(pet2Button);
		frame.getContentPane().add(pet3Button);
		frame.getContentPane().add(txtPlayerPets);
		frame.getContentPane().add(txtPlayerPets_1);
		frame.getContentPane().add(pet4Button);
		frame.getContentPane().add(pet5Button);
		frame.getContentPane().add(pet6Button);
		frame.getContentPane().add(txtPlayerPets_2);
		frame.getContentPane().add(pet8Button);
		frame.getContentPane().add(pet9Button);
		frame.getContentPane().add(pet7Button);


		// Current pet text
		txtPet = new JTextField();
		txtPet.setHorizontalAlignment(SwingConstants.CENTER);
		txtPet.setEditable(false);
		txtPet.setText("No pet selected");
		txtPet.setBounds(367, 405, 210, 32);
		frame.getContentPane().add(txtPet);
		txtPet.setColumns(10);

		txtDayCount = new JTextField();
		txtDayCount.setEditable(false);
		txtDayCount.setText("Day count: "+Game.daycount);
		txtDayCount.setBounds(191, 443, 145, 33);
		frame.getContentPane().add(txtDayCount);
		txtDayCount.setColumns(10);

		//Shows Players Turn
		txtPlayersTurn = new JTextField();
		txtPlayersTurn.setFont(new Font("Dialog", Font.BOLD, 12));
		txtPlayersTurn.setEditable(false);
		txtPlayersTurn.setText("Player Turn: Player 1 ");
		txtPlayersTurn.setColumns(10);
		txtPlayersTurn.setBounds(207, 12, 129, 51);
		frame.getContentPane().add(txtPlayersTurn);

		txtActionsLeft = new JTextField();
		txtActionsLeft.setEditable(false);
		txtActionsLeft.setText("Actions Left: "+ 2*Game.player1PetNum);
		txtActionsLeft.setColumns(10);
		txtActionsLeft.setBounds(207, 92, 129, 33);
		frame.getContentPane().add(txtActionsLeft);

		player1Name = new JTextField();
		player1Name.setText(Game.player1Name);
		player1Name.setFont(new Font("Tahoma", Font.BOLD, 12));
		player1Name.setEditable(false);
		player1Name.setColumns(10);
		player1Name.setBounds(392, 3, 85, 33);
		frame.getContentPane().add(player1Name);

		player2Name = new JTextField();
		player2Name.setFont(new Font("Tahoma", Font.BOLD, 12));
		player2Name.setText(Game.player2Name);
		player2Name.setEditable(false);
		player2Name.setColumns(10);
		player2Name.setBounds(506, 3, 85, 33);
		frame.getContentPane().add(player2Name);

		player3Name = new JTextField();
		player3Name.setFont(new Font("Tahoma", Font.BOLD, 12));
		player3Name.setText(Game.player3Name);
		player3Name.setEditable(false);
		player3Name.setColumns(10);
		player3Name.setBounds(618, 3, 85, 33);
		frame.getContentPane().add(player3Name);



		txtMoney = new JTextField();
		txtMoney.setEditable(false);
		txtMoney.setText("Money: " +Players.player1[0]);
		txtMoney.setBounds(207, 125, 129, 30);
		frame.getContentPane().add(txtMoney);
		txtMoney.setColumns(10);

		PetActions = new JTextField();
		PetActions.setText("0 Actions");
		PetActions.setHorizontalAlignment(SwingConstants.CENTER);
		PetActions.setEditable(false);
		PetActions.setColumns(10);
		PetActions.setBounds(589, 405, 129, 32);
		frame.getContentPane().add(PetActions);

		GameDialog = new TextArea();
		GameDialog.setText("Game Dialog:\r\n");
		GameDialog.setBounds(28, 495, 739, 197);
		frame.getContentPane().add(GameDialog);

		txtScore = new JTextField();
		txtScore.setText("Score: 0");
		txtScore.setEditable(false);
		txtScore.setColumns(10);
		txtScore.setBounds(207, 155, 129, 30);
		frame.getContentPane().add(txtScore);

		weatherStatusText = new JTextField();
		weatherStatusText.setText("Weather: Sunny");
		weatherStatusText.setEditable(false);
		weatherStatusText.setColumns(10);
		weatherStatusText.setBounds(207, 203, 129, 40);
		frame.getContentPane().add(weatherStatusText);

		txtDaysPlaying = new JTextField();
		txtDaysPlaying.setText("Days Playing: "+ Game.daysToPlay);
		txtDaysPlaying.setEditable(false);
		txtDaysPlaying.setColumns(10);
		txtDaysPlaying.setBounds(191, 405, 145, 33);
		frame.getContentPane().add(txtDaysPlaying);

		weatherIcon = new JLabel("");
		weatherIcon.setIcon(new ImageIcon(Gui.class.getResource("/Pictures/Sunny.png")));
		weatherIcon.setBounds(207, 266, 130, 100);
		frame.getContentPane().add(weatherIcon);

		txtPlayerStats = new JTextField();
		txtPlayerStats.setText("Player Stats: Player 1");
		txtPlayerStats.setEditable(false);
		txtPlayerStats.setColumns(10);
		txtPlayerStats.setBounds(207, 61, 129, 33);
		frame.getContentPane().add(txtPlayerStats);

		txtAge = new JTextField();
		txtAge.setEditable(false);
		txtAge.setHorizontalAlignment(SwingConstants.CENTER);
		txtAge.setText("D.O.B");
		txtAge.setBounds(740, 405, 52, 33);
		frame.getContentPane().add(txtAge);
		txtAge.setColumns(10);

		dobTxt = new JTextField();
		dobTxt.setEditable(false);
		dobTxt.setHorizontalAlignment(SwingConstants.CENTER);
		dobTxt.setText(Pets.getDOB());
		dobTxt.setColumns(10);
		dobTxt.setBounds(733, 444, 73, 33);
		frame.getContentPane().add(dobTxt);
		
		txtPetSick = new JTextField();
		txtPetSick.setEditable(false);
		txtPetSick.setText("Pet Sick?");
		txtPetSick.setBounds(647, 368, 74, 24);
		frame.getContentPane().add(txtPetSick);
		txtPetSick.setColumns(10);
		
		petSick = new JTextField();
		petSick.setEditable(false);
		petSick.setText("No");
		petSick.setColumns(10);
		petSick.setBounds(740, 368, 36, 24);
		frame.getContentPane().add(petSick);

		//Whole Jframe actions:



	}
}
