package tamagotchi;


	/**
	 *  Player class that holds player related methods and player items
	 * @author Jamie
	 *
	 *
	 */
public class Players {



	// {Money, Score,player number,actions left}
		/**
		 * integer list that holds player 1
		 */
	static int[] player1 = {20*Game.player1PetNum*Game.daysToPlay,0,1,Game.player1PetNum*2};
		/**
		 * integer list that holds player 2
		 */
	static int[] player2 = {20*Game.player2PetNum*Game.daysToPlay,0,2,Game.player2PetNum*2};
		/**
		 * integer list that holds player 3
		 */
	static int[] player3 = {20*Game.player3PetNum*Game.daysToPlay,0,3,Game.player3PetNum*2};

	/**
	 * Calculates the score of player1
	 * @param player is a int[] list
	 * @param petnum is a int[] list
	 */

	static void calculatescorePlayer1(final int[] player, final int petnum){

		if (petnum > 0)
		{
			player[1] = player[1] + Game.petList.get(0)[1]; //hunger
			player[1] = player[1] + Game.petList.get(0)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(0)[3]; //mood
		}
		else if (petnum > 1)
		{
			player[1] = player[1] + Game.petList.get(0)[1]+Game.petList.get(1)[1]; //hunger
			player[1] = player[1] + Game.petList.get(0)[2]+Game.petList.get(1)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(0)[3]+Game.petList.get(1)[3]; //mood
		}
		else if (petnum > 2) // must be 3 pets
		{
			player[1] = player[1] + Game.petList.get(0)[1]+Game.petList.get(1)[1]+Game.petList.get(2)[1]; //hunger
			player[1] = player[1] + Game.petList.get(0)[2]+Game.petList.get(1)[2]+Game.petList.get(2)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(0)[3]+Game.petList.get(1)[3]+Game.petList.get(2)[3]; //mood
		}


	}

	/**
	 * Calculates the score of player 2
	 * @param player  is a int[] list
	 * @param petnum  is a int[] list
	 */

	static void calculatescorePlayer2(final int[] player, final int petnum){

		if (petnum > 0)
		{
			player[1] = player[1] + Game.petList.get(3)[1]; //hunger
			player[1] = player[1] + Game.petList.get(3)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(3)[3]; //mood
		}
		else if (petnum > 1)
		{
			player[1] = player[1] + Game.petList.get(3)[1]+Game.petList.get(4)[1]; //hunger
			player[1] = player[1] + Game.petList.get(3)[2]+Game.petList.get(4)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(3)[3]+Game.petList.get(4)[3]; //mood
		}
		else if (petnum > 2)
		{
			player[1] = player[1] + Game.petList.get(3)[1]+Game.petList.get(4)[1]+Game.petList.get(5)[1]; //hunger
			player[1] = player[1] + Game.petList.get(3)[2]+Game.petList.get(4)[2]+Game.petList.get(5)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(3)[3]+Game.petList.get(4)[3]+Game.petList.get(5)[3]; //mood
		}


	}
	
		/**
		 * Calculates the score of player 3
		 * @param player  is a int[] list
		 * @param petnum  is a int[] list
		 */

	static void calculatescorePlayer3(final int[] player, final int petnum){

		if (petnum > 0)
		{
			player[1] = player[1] + Game.petList.get(6)[1]; //hunger
			player[1] = player[1] + Game.petList.get(6)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(6)[3]; //mood
		}
		else if (petnum > 1)
		{
			player[1] = player[1] + Game.petList.get(6)[1]+ 
					Game.petList.get(7)[1]; //hunger
			player[1] = player[1] + Game.petList.get(6)[2]+ 
					Game.petList.get(7)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(6)[3]+ 
					Game.petList.get(7)[3]; //mood
		}
		else if (petnum > 2)// Must be 3 pets
		{
			player[1] = player[1] + Game.petList.get(6)[1]+ 
					Game.petList.get(7)[1]+ Game.petList.get(8)[1]; //hunger
			player[1] = player[1] + Game.petList.get(6)[2]+ 
					Game.petList.get(7)[2]+ Game.petList.get(8)[2]; //tiredness	
			player[1] = player[1] + Game.petList.get(6)[3]+ 
					Game.petList.get(7)[3]+ Game.petList.get(8)[3]; //mood
		}


	}
	
	static int getPlayerActions(final int[] player){
		int playerActions = 0;
		try {
			if (player[2] == 1){
				playerActions = playerActions + Game.petList.get(0)[5];
				playerActions = playerActions + Game.petList.get(1)[5];
				playerActions = playerActions + Game.petList.get(2)[5];	
				return playerActions;
			}
			else if (player[2] == 2){
				playerActions = playerActions + Game.petList.get(3)[5];
				playerActions = playerActions + Game.petList.get(4)[5];
				playerActions = playerActions + Game.petList.get(5)[5];	
				return playerActions;
			}
			else if (player[2] == 3){
				playerActions = playerActions + Game.petList.get(6)[5];
				playerActions = playerActions + Game.petList.get(7)[5];
				playerActions = playerActions + Game.petList.get(8)[5];
				return playerActions;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// catch index errors for pets that dont exist
		}
		return playerActions;
		
		
		
	}





	/**
	 * Calculates which player won and if there was a draw
	 * then return the result
	 * @param player1 is a int[] list
	 * @param player2 is a int[] list
	 * @param player3 is a int[] list
	 * @return winingPlayer is a String
	 */
	static String calculateWinner(final int[] player1,  final int[] player2,  final int[] player3){
		if(player1[1]> player2[1] && player1[1]> player3[1]){
			return  "Player 1";
		}

		else if(player2[1]> player1[1] && player2[1]> player3[1]){	
			return  "Player 2";
		}

		else if(player3[1]> player1[1] && player3[1]> player2[1]){
			return  "Player 3";

		}
		return "Everyone! there was a draw";
	}
}
