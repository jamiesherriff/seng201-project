package tamagotchi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

	/**
	 * Pet class that holds all pet related actions
	 * @author Jamie
	 *
	 */
public class Pets {
	
	// int[] defaultPet = {50,50,50,50,5,2,0,0};
	//static int[] tamatchiPet = {40,80,30,30,8,2,0,0};
	//static int[] maskutchiPet = {50,40,70,50,4,2,0,0};
	//static int[] mametachiPet = {50,50,40,60,6,2,0,0};
	static  int[] emptyPet = {};
	

		/**
		 * Prints out an individual pet attributes
		 * @param pet
		 */
	final void GetPetStats(final int[] pet){
		System.out.println("health: "+ pet[0]+"");
		System.out.println("hunger: "+ pet[1]+" ");
		System.out.println("energy: "+ pet[2]+" " );
		System.out.println("mood: "+ pet[3]+ " " );
		System.out.println("Weight: "+ pet[4]);
		System.out.println("Actions left: "+ pet[5]);
		System.out.println("Is sick?: "+ pet[6]);
		System.out.println("Recieved attention: "+ pet[7]);
		System.out.println("Belongs to player number: "+ pet[8]);
		System.out.println("Speicies Number: "+ pet[9]);
		System.out.println("Has the pet been revived?: "+ pet[10]);

	}
	
	/**
	 * Checks if pet is sick
	 * @param pet
	 * @return returns a string yes or no
	 * 
	 */
	final static String isPetSick(final int[] pet){
		if (pet[6]==1){
			return "Yes";
		}
		return "No";
	}
		
		
		/**
		 * Updates pets for the end of the day
		 * Drops mood,energy and hunger
		 */
	static void endOfDay(){
		for (int i = 0; i < Game.petList.size(); i++) {
			if(Game.petList.get(i).length==0){
			}	else	{
			for (int j = 0; j < Game.petList.get(i)[j]; j++) {
				Game.petList.get(i)[4] = (Game.petList.get(i)[4] -1);
				Game.petList.get(i)[1] = (Game.petList.get(i)[1] -2) ;
				Game.petList.get(i)[2] = (Game.petList.get(i)[2] -2) ;
				Game.petList.get(i)[3] = (Game.petList.get(i)[3] -2) ;
			}
			}
		}
		
		
	}
		/**
		 * returns The current day when the pets were born
		 * @return  String date of birth dd/MM/yyyy
		 */
	static String getDOB(){	
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String birthDate = (dateFormat.format(date));
		return birthDate.toString();
	}

	/**
	 * Updates pet list for weather changes
	 * @param weather
	 */
static void weatherPetUpdate(final String weather){
	for (int i = 0; i < Game.petList.size(); i++) {
		if(Game.petList.get(i).length==0){
		}	else	{
		for (int j = 0; j < Game.petList.get(i)[j]; j++) {
			if (weather == "Sunny"){
				Game.petList.get(i)[3]= Game.petList.get(i)[3]+10;
			}
			if (weather == "Raining"){
				Game.petList.get(i)[3]= Game.petList.get(i)[3]-1;				
			}
		}
		}
	}
	if (weather == "Raining")	{
		System.out.println("Pets are depressed becuase of the rain!");
	}	else if (weather == "Sunny")	{
		System.out.println("Pets are happy becuase of the sunshine!");
	}	else if (weather == "Cloudy")	{
		System.out.println("Pets have no change because its cloudy!");
	}
		
	}
		

	/**
	 * Bring up a prompt asking the user for what food to pick
	 * @return the selected food string the user has chosen
	 */
	static String getFood(){
		String[] possiblefoods = { "Dinner - Costs $25", "Snack - Costs $10", "Medicine - Costs $50" };	
		String food = (String) JOptionPane.showInputDialog(null,
				"Please select a food for your pet","Food Selection",
				JOptionPane.QUESTION_MESSAGE, null,
				possiblefoods, possiblefoods[0]);

		return food;	
	}
	
		/**
		 * Bring up a prompt asking the user for what interaction to pick 
		 * @return the selected interaction string the user has chosen
		 */
	static String getPlayOptions(){
		String[] possibleInteractions = { "Play with disposable soft toy - Costs $5", "Go to Toilet", "Cuddle Pet", "Take for a walk"};	
		String food = (String) JOptionPane.showInputDialog(null,
				"Please select an action for your pet","Pet Interaction",
				JOptionPane.QUESTION_MESSAGE, null,
				possibleInteractions, possibleInteractions[0]);

		return food;	
	}
	
	/**
	 * Resets the pet actions and jealousy
	 * loops through the Entire game petlist
	 */
	static void resetPetActions(){
			try {

				for (int i = 0; i < Game.petList.size(); i++) {
					if(Game.petList.get(i).length==0){
					}	else	{
					for (int j = 0; j < Game.petList.get(i)[j]; j++) {
						Game.petList.get(i)[5] = 2;
						Game.petList.get(i)[7] = 0;
					}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	
		/**
		 * Loops through the entire game petlist
		 * resets pet jealousy only
		 */
	static void resetJealousy(){
		try {

			for (int i = 0; i < Game.petList.size(); i++) {
				if(Game.petList.get(i).length==0){
				}	else	{
				for (int j = 0; j < Game.petList.get(i)[j]; j++) {
					Game.petList.get(i)[7] = 0;
				}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
		/**
		 * Revives the currently selected pet
		 * Puts up a message if no pet is selected
		 * @param pet
		 * @param player
		 * @param currentPetName
		 * @param currentPlayerName
		 * @param currentPlayer
		 */
	static void revivePet(final int[] pet, final int[] player, final String currentPetName, final String currentPlayerName, final int[] currentPlayer){
		if (pet == null || Gui.currentPlayer == null)	{
			JOptionPane.showMessageDialog(null, "Please Select a Pet");
		}
		else{
		
		if (pet[10]==1)
		{
			JOptionPane.showMessageDialog(null, "This pet has already been revived! Sorry but its gone.");
		}	else if (currentPlayer[2] != Game.playerPostion){
			JOptionPane.showMessageDialog(null, "This is not your pet!");
			System.out.println("Player " + Game.playerPostion + " Tried to revive someone elses Pet!");
		}	else	{
		pet[10]=1;
		pet[0]=50;
		pet[1]=50;
		pet[2]=50;
		pet[3]=50;
		pet[4]=5;
		pet[5]=0;
		currentPlayer[1]=currentPlayer[1]-500;
		currentPlayer[0]=currentPlayer[0]-50;
		System.out.println(currentPlayerName+ " Has revivied " + currentPetName);
		}
		}
	}
	
	


	/**
	 * Ensure that if pet attribute values go out of bounds they are reset
	 * loops through pet list
	 */
static void updatePetLimits(){
	try {
		for (int i = 0; i < Game.petList.size(); i++) {
			if(Game.petList.get(i).length==0){
				i++;
			}	else	{
			for (int j = 0; j < 4; j++) {
				if (Game.petList.get(i)[j] > 100) {
					Game.petList.get(i)[j] = 100;
				}
				if (Game.petList.get(i)[j] < 0) {
					Game.petList.get(i)[j] = 0;
				}
			}
			for (int j = 4; j < 5; j++) {
				if (Game.petList.get(i)[j] > 10) {
					Game.petList.get(i)[j] = 10;
				}
				if (Game.petList.get(i)[j] < 0) {
					Game.petList.get(i)[j] = 1;
				}
			}
			}
			}
	} catch (Exception e) {
		// TODO: handle exception
	}	
}

	/**
	 * Feeds the selected pet
	 * If no pet is selected a message is shown
	 * 
	 * @param pet
	 * @param player
	 * @param food
	 * @param currentPetName
	 * @param currentPlayerName
	 * @param currentPlayer
	 */
	static void feedpet(final int[] pet, final int[] player, final String food, final String currentPetName, final String currentPlayerName, final int[] currentPlayer){
		if (pet == null || Gui.currentPlayer == null)	{
			JOptionPane.showMessageDialog(null, "Please Select a Pet");
		}	else if (currentPlayer[2] != Game.playerPostion){
			JOptionPane.showMessageDialog(null, "This is not your pet!");
			System.out.println("Player " + Game.playerPostion + " Tried to feed someone elses Pet!");	
		}	else if (currentPlayer[0] <= 0)	{
			JOptionPane.showMessageDialog(null, "You have no money left!");
			return;
		}	else if (pet[5]<=0)	{
			JOptionPane.showMessageDialog(null, "No actions left for this pet");
			return;
		}	else if(pet[1] >= 100)	{
			JOptionPane.showMessageDialog(null, "This pet can not eat anymore");
		}	else if(pet[0] >= 100)	{
			JOptionPane.showMessageDialog(null, "This pet can not get any healthier!");	
		}	else	{
			if(food == "Dinner - Costs $25")
			{
				pet[0] = pet[0]+15;
				pet[1] = pet[1]+25;
				pet[5] = pet[5]-1;
				pet[4] = pet[4] +4;
				pet[4] = pet[4] +2;
				pet[7] = pet[7]+ 1;
				player[0]= player[0] -25;
				currentPlayer[3]=currentPlayer[3]-3;
				System.out.println(currentPlayerName+ " Has feed dinner to " + currentPetName);
			}
			if (food == "Snack - Costs $10")	{
				pet[0] = pet[0]+2;
				pet[1]= pet[1]+5;
				pet[5] = pet[5]-1;
				player[0]= player[0] -10;
				pet[4] = pet[4] +1;
				pet[7] = pet[7]+ 1;
				currentPlayer[3]=currentPlayer[3]-3;
				System.out.println(currentPlayerName+ " Has feed a snack to " + currentPetName);
			}
			if (food == "Medicine - Costs $50")	{
				pet[6] = 0;
				pet[0] = pet[0]+10;
				player[0]= player[0] -50;
				currentPlayer[3]=currentPlayer[3]-3;
				System.out.println(currentPlayerName+ " Has given to medicine " + currentPetName);
			}
			if (pet[0]>100)	{ //adjusting for case if health exceeds 100
				pet[0] = 100;
			}
			if (pet[1] >100){ //adjusting for case if hunger exceeds 100
				pet[1] =100;
			}
		}
		Gui.currentPlayer = player;
	}
	
	

	/**
	 * Sleeps the selected pet
	 * If no pet is selected a message is shown
	 * @param pet
	 * @param player
	 * @param currentPetName
	 * @param currentPlayerName
	 * @param currentPlayer
	 */
	static void sleepPet(final int[] pet,final int[] player, final String currentPetName, final String currentPlayerName, final int[]currentPlayer){
		if (pet == null || Gui.currentPlayer == null){
			JOptionPane.showMessageDialog(null, "Please Select a Pet");
		}	else if (currentPlayer[2] != Game.playerPostion){
			JOptionPane.showMessageDialog(null, "This is not your pet!");
			System.out.println("Player " + Game.playerPostion+ " Tried to sleep someone elses Pet!");	
		}	else if (pet[5]<=0){
			JOptionPane.showMessageDialog(null, "No actions left for this pet");			
		}	else if	(pet[2] >= 100){
			JOptionPane.showMessageDialog(null, "This pet can not sleep anymore!");
		}	else	{
		pet[2] = pet[2] + 20;
		pet[5] = pet[5]-1; // Decrease Pet actions
		pet[7] = pet[7]+ 1;
		currentPlayer[3]=currentPlayer[3]-3; // Decrease player actions
		System.out.println(currentPlayerName+ " has caused " + currentPetName+ " to sleep");
		}
	}

		/**
		 * Plays with the selected pet
		 * If no pet is selected a message is shown
		 * @param pet
		 * @param toy
		 * @param currentPetName
		 * @param currentPlayerName
		 * @param currentPlayer
		 */
	static void interactPet(final int[] pet, final String toy,final String currentPetName, final String currentPlayerName,final int[]currentPlayer){
		if (pet == null || Gui.currentPlayer == null){
			JOptionPane.showMessageDialog(null, "Please Select a Pet");
		}	else if (currentPlayer[2] != Game.playerPostion)	{
			JOptionPane.showMessageDialog(null, "This is not your pet!");
			System.out.println("Player " + Game.playerPostion+ " Tried to interact someone elses Pet!");
		}	else if (pet[5]<=0)	{
			JOptionPane.showMessageDialog(null, "No actions left for this pet");
			
		}	else if	(pet[2] >= 100)	{
			JOptionPane.showMessageDialog(null, "This pet can not sleep anymore!");
		}	else if	(pet[3] >= 100)	{
			JOptionPane.showMessageDialog(null, "This pet is too happy!");
		}	else	{
			if	(toy == "Play with disposable soft toy - Costs $5"){
				pet[2] = pet[2]- 10;
				pet[3] = pet[3]+ 20;
				pet[5] = pet[5]- 1;
				pet[7] = pet[7]+ 1;
				currentPlayer[3]=currentPlayer[3]-3;
				System.out.println(currentPlayerName+ " Has given a soft toy to " + currentPetName);
			}
			if	(toy == "Go to Toilet"){
				pet[4] = pet[4]- 2;
				pet[3] = pet[3]+ 10;
				pet[5] = pet[5]- 1;
				currentPlayer[3]=currentPlayer[3]-3;
				System.out.println(currentPlayerName+ " Has made " + currentPetName+ " go to the toilet");
			}
			if	(toy == "Cuddle Pet"){
				pet[3] = pet[3] + 15;
				pet[5] = pet[5]- 1;
				pet[7] = pet[7]+ 1;
				currentPlayer[3]=currentPlayer[3]-3;
				System.out.println(currentPlayerName+ " Has cuddled " + currentPetName);
			}
			if	(toy == "Take for a walk"){
				pet[3] = pet[3] + 20;
				pet[2] = pet[2]- 10;
				pet[4] = pet[4]- 1;
				pet[5] = pet[5]- 1;
				pet[7] = pet[7]+ 1;
				currentPlayer[3]=currentPlayer[3]-3;
				System.out.println(currentPlayerName + " Has taken " + currentPetName+ " for a walk");
			}
			}
	}
	
		/**
		 * Checks to see if a pet has received attention for player 1
		 * If it has it increments the mood the current pet and decreases all players pets
		 * 
		 */
	 static void player1PetJealousy(){
		try {
			for (int i = 0; i < 3; i++) {
				for (int j = 7; j < 8 ; j++) {
					// looks at the jealously index and if there is more than one pet
					
					if(Game.petList.get(i)[7] >=1 && Game.speciesList1.size()> 1){
						System.out.println("Player 1 has made other pets jealous!");
						Game.petList.get(i)[3]=Game.petList.get(i)[3]+3;
							Game.petList.get(0)[3]=Game.petList.get(0)[3]-2;
							Game.petList.get(1)[3]=Game.petList.get(1)[3]-2;
							Game.petList.get(2)[3]=Game.petList.get(2)[3]-2;
							
						}
						
					}
				}
		} catch (ArrayIndexOutOfBoundsException e) {
			// Catches index error exceptions
			System.out.println("Player 1 Is unable to make other pets jealous!");
		}
		resetJealousy();
		}
	 
	 	/**
	 	 * Checks to see if a pet has received attention for player 2
	 	 * If it has it increments the mood the current pet and decreases all players pets
	 	 */
	 static void player2PetJealousy(){
		try {
			for (int i = 3; i < 6; i++) {
				for (int j = 7; j < 8 ; j++) {
					// looks at the jealously index and if there is more than one pet
					
					if(Game.petList.get(i)[7] >=1 && Game.speciesList1.size()> 1){
						System.out.println("Player 2 has made other pets jelous!");
						Game.petList.get(i)[3]=Game.petList.get(i)[3]+3;
							Game.petList.get(3)[3]=Game.petList.get(3)[3]-2;
							Game.petList.get(4)[3]=Game.petList.get(4)[3]-2;
							Game.petList.get(5)[3]=Game.petList.get(5)[3]-2;
							
						}
						
					}
				}
		} catch (ArrayIndexOutOfBoundsException e) {
			// Catches index error exceptions
			System.out.println("Player 2 Is unable to make other pets jealous!");
		}
		resetJealousy();
		}
	 
	 	/**
	 	 * Checks to see if a pet has received attention for player 1
	 	 * If it has it increments the mood the current pet and decreases all players pets
	 	 */
	 static void player3PetJealousy(){
		try {
			for (int i = 6; i < 9; i++) {
				for (int j = 7; j < 8 ; j++) {
					// looks at the jealously index and if there is more than one pet
					
					if(Game.petList.get(i)[7] >=1 && Game.speciesList1.size()> 1){
						System.out.println("Player 3 has made other pets jealous!");
						Game.petList.get(i)[3]=Game.petList.get(i)[3]+3;
							Game.petList.get(6)[3]=Game.petList.get(6)[3]-2;
							Game.petList.get(7)[3]=Game.petList.get(7)[3]-2;
							Game.petList.get(8)[3]=Game.petList.get(8)[3]-2;
							
						}
						
					}
				}
		} catch (ArrayIndexOutOfBoundsException e) {
			// Catches index error exceptions
			System.out.println("Player 3 Is unable to make other pets jealous!");
		}
		resetJealousy();
		}
	 
			/**
			 * Compares the current player to player number
			 * chooses the appropriate petJealousy to apply
			 * @param player
			 */
		static void petJealousy(int[] player){
			if(player[2]==1){
				player1PetJealousy();
			}	else if (player[2]==2){
				player2PetJealousy();
			}	else if (player[2]==3){
				player3PetJealousy();
			}
			
			
		}
	
}




