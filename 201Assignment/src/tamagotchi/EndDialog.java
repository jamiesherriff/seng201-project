package tamagotchi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

	/**
	 * End Dialog that shows at the end of game
	 *Shows the winning player and everyones score
	 * @author Jamie
	 *
	 */
public class EndDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField player1Name;
	private JTextField txtWinner;
	private JTextField player2Name;
	private JTextField player3Name;
	private JTextField player1Score;
	private JTextField player2Score;
	private JTextField player3Score;
	private JTextField winingPlayer;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			EndDialog dialog = new EndDialog();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public EndDialog() {
		setBounds(500, 300, 497, 375);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			player1Name = new JTextField();
			player1Name.setBounds(108, 132, 116, 22);
			player1Name.setEditable(false);
			player1Name.setText(Game.player1Name +" 's Score");
			player1Name.setColumns(10);
		}
		

		
		txtWinner = new JTextField();
		txtWinner.setEditable(false);
		txtWinner.setBounds(44, 18, 386, 55);
		txtWinner.setHorizontalAlignment(SwingConstants.CENTER);
		txtWinner.setText("WINNER!");
		txtWinner.setFont(new Font("Tahoma", Font.BOLD, 40));
		txtWinner.setColumns(10);
		
		player2Name = new JTextField();
		player2Name.setBounds(108, 167, 116, 22);
		player2Name.setEditable(false);
		player2Name.setText(Game.player2Name +" 's Score");
		player2Name.setColumns(10);
		
		player3Name = new JTextField();
		player3Name.setBounds(108, 202, 116, 22);
		player3Name.setEditable(false);
		player3Name.setText(Game.player3Name +" 's Score");
		player3Name.setColumns(10);
		
		player1Score = new JTextField();
		player1Score.setBounds(249, 132, 116, 22);
		player1Score.setEditable(false);
		player1Score.setText(""+Players.player1[1]);
		player1Score.setColumns(10);
		
		player2Score = new JTextField();
		player2Score.setBounds(249, 167, 116, 22);
		player2Score.setEditable(false);
		player2Score.setText(""+Players.player2[1]);
		player2Score.setColumns(10);
		
		player3Score = new JTextField();
		player3Score.setBounds(249, 202, 116, 22);
		player3Score.setEditable(false);
		player3Score.setText(""+Players.player3[1]);
		player3Score.setColumns(10);
		
		winingPlayer = new JTextField();
		winingPlayer.setEditable(false);
		winingPlayer.setBounds(54, 86, 357, 33);
		winingPlayer.setFont(new Font("Tahoma", Font.BOLD, 15));
		winingPlayer.setHorizontalAlignment(SwingConstants.CENTER);
		winingPlayer.setText("Congratulations "+ Game.winningPlayer);
		winingPlayer.setColumns(10);
		contentPanel.setLayout(null);
		contentPanel.add(txtWinner);
		contentPanel.add(winingPlayer);
		contentPanel.add(player1Name);
		contentPanel.add(player1Score);
		contentPanel.add(player2Name);
		contentPanel.add(player2Score);
		contentPanel.add(player3Name);
		contentPanel.add(player3Score);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						JOptionPane.showMessageDialog(null, "Thanks For Plaiyng!");
						System.exit(0);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JOptionPane.showMessageDialog(contentPanel, "Thanks For Plaiyng!");
						System.exit(0);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		if (Game.numPlayers == 1){
			player2Name.setText("");
			player2Name.setEnabled(false);
			player2Score.setText("");
			player3Name.setText("");
			player3Name.setEnabled(false);
			player3Score.setText("");
			
		}
		else if (Game.numPlayers == 2){
			player3Name.setText("");
			player3Name.setEnabled(false);
			player3Score.setText("");
		}
		
	}
}
