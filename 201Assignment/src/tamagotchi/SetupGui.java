package tamagotchi;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

	/**
	 * Setup Gui that players can customize
	 * @author Jamie
	 *
	 */
public class SetupGui {

	JFrame setupGui;
	private JTextField txtNumberOfPlayers;
	private JTextField txtPleaseEnterYour;
	private JTextField txtPlayer;
	private JTextField txtPlayer_1;
	private JTextField txtPlayer_2;
	private JTextField player1Name;
	private JTextField player2Name;
	private JTextField player3Name;
	private JTextField txtGameSetup;
	private JTextField enterPetNameText;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField player1Pet1Name;
	private JTextField textField_4;
	private JTextField txtPet;
	private JTextField player1Pet2Name;
	private JTextField txtPet_1;
	private JTextField player1Pet3Name;
	private JTextField player2Pet1Name;
	private JTextField player2Pet2Name;
	private JTextField player2Pet3Name;
	private JTextField player3Pet1Name;
	private JTextField player3Pet2Name;
	private JTextField player3Pet3Name;
	private JTextField textField_18;
	private JTextField textField_19;
	private JTextField textField_26;
	private JTextField textField_27;
	private JTextField textField_28;
	private JTextField textField_29;
	private JTextField pleaseEnterSpeciesText;
	private JComboBox player1Pet1Species;
	private JComboBox player1Pet2Species;
	private JComboBox player1Pet3Species;
	private JComboBox player2Pet1Species;
	private JComboBox player2Pet2Species;
	private JComboBox player2Pet3Species;
	private JComboBox player3Pet1Species;
	private JComboBox player3Pet2Species;
	private JComboBox player3Pet3Species;
	private JTextField daysToPlayText;
	private JTextField daysToPlay;
	private JButton button;
	private JComboBox comboPlayer2PetNum;
	private JComboBox comboPlayer1PetNum;
	private JComboBox comboPlayer3PetNum;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					SetupGui window = new SetupGui();
//					window.setupGui.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 * Initialize the contents of the frame.
	 */
	public SetupGui() {
		initialize();
	}


	/**
	 * Checks every player name to see if there is a duplicate
	 * @param Name1 PlayerName1
	 * @param Name2 PlayerName2
	 * @param Name3 PlayerName3
	 * @return true or false
	 */
	boolean checkPlayerNames(String Name1, String Name2, String Name3){
		if(Name1.equals(Name2) ){
			return true;
		}else if (Name1.equals(Name3))	{
			return true;
		}else if (Name2.equals(Name3))	{
			return true;
		}	else	{
			return false;
		}


	}
		/**
		 * 
		 * @param p1P1Name
		 * @param p1P2Name
		 * @param p1P3Name
		 * @param p2P1Name
		 * @param p2P2Name
		 * @param p2P3Name
		 * @param p3P1Name
		 * @param p3P2Name
		 * @param p3P3Name
		 * @return Boolean statement if there duplicate pet names for an individual player
		 */

	boolean checkPetNames(JTextField p1P1Name, JTextField p1P2Name, JTextField p1P3Name,
			JTextField p2P1Name, JTextField p2P2Name, JTextField p2P3Name,
			JTextField p3P1Name, JTextField p3P2Name, JTextField p3P3Name)	{
		if(p1P1Name.getText().equals(p1P2Name.getText()))	{
			return true;
		}else if (p1P1Name.getText().equals(p1P3Name.getText()))	{
			return true; 
		}else if (p1P3Name.getText().equals(p1P2Name.getText()))	{
			return true; 
		}else if (p2P1Name.getText().equals(p2P2Name.getText()))	{
			return true; 
		}else if (p2P1Name.getText().equals(p2P3Name.getText()))	{
			return true; 
		}else if (p2P3Name.getText().equals(p2P2Name.getText()))	{
			return true; 
		}else if (p3P1Name.getText().equals(p3P2Name.getText()))	{
			return true; 
		}else if (p3P1Name.getText().equals(p3P3Name.getText()))	{
			return true; 
		}else if (p3P3Name.getText().equals(p3P2Name.getText()))	{
			return true;
		}

		return false;

	}
	
	/**
	 * initializes the SetupGui frame
	 */
	private void initialize() {
		setupGui = new JFrame();
		setupGui.setTitle("Tamagotchi Game Setup");
		setupGui.getContentPane().setEnabled(false);
		setupGui.setMinimumSize(new Dimension(600, 700));
		setupGui.setBounds(500, 300, 600, 700);
		setupGui.setResizable(false);
		setupGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		txtNumberOfPlayers = new JTextField();
		txtNumberOfPlayers.setBounds(119, 43, 238, 36);
		txtNumberOfPlayers.setEditable(false);
		txtNumberOfPlayers.setHorizontalAlignment(SwingConstants.LEFT);
		txtNumberOfPlayers.setText("Number Of players to play Tamagotchi");
		txtNumberOfPlayers.setColumns(10);

		txtPleaseEnterYour = new JTextField();
		txtPleaseEnterYour.setBounds(89, 83, 389, 28);
		txtPleaseEnterYour.setEditable(false);
		txtPleaseEnterYour.setText("Please enter your player name and number of pets below");
		txtPleaseEnterYour.setHorizontalAlignment(SwingConstants.CENTER);
		txtPleaseEnterYour.setColumns(10);

		txtPlayer = new JTextField();
		txtPlayer.setBounds(137, 122, 55, 22);
		txtPlayer.setEditable(false);
		txtPlayer.setText("Player 1");
		txtPlayer.setColumns(10);

		player1Name = new JTextField();
		player1Name.setBounds(194, 122, 166, 22);
		player1Name.setText("Player 1");
		player1Name.setColumns(10);
		player1Name.getText();

		txtPlayer_1 = new JTextField();
		txtPlayer_1.setBounds(137, 155, 50, 22);
		txtPlayer_1.setEditable(false);
		txtPlayer_1.setText("Player 2");
		txtPlayer_1.setColumns(10);

		txtPlayer_2 = new JTextField();
		txtPlayer_2.setBounds(137, 188, 50, 22);
		txtPlayer_2.setEditable(false);
		txtPlayer_2.setText("Player 3");
		txtPlayer_2.setColumns(10);

		player2Name = new JTextField();
		player2Name.setBounds(194, 155, 166, 22);
		player2Name.setText("Player 2");
		player2Name.setColumns(10);

		player3Name = new JTextField();
		player3Name.setBounds(194, 188, 166, 22);
		player3Name.setText("Player 3");
		player3Name.setColumns(10);

		player1Pet1Name = new JTextField();
		player1Pet1Name.setText("Pet 1");
		player1Pet1Name.setBounds(194, 289, 73, 22);
		player1Pet1Name.setColumns(10);

		player1Pet2Name = new JTextField();
		player1Pet2Name.setText("Pet 2");
		player1Pet2Name.setBounds(282, 289, 73, 22);
		player1Pet2Name.setColumns(10);

		player1Pet3Name = new JTextField();
		player1Pet3Name.setText("Pet 3");
		player1Pet3Name.setBounds(365, 289, 73, 22);
		player1Pet3Name.setColumns(10);

		player2Pet1Name = new JTextField();
		player2Pet1Name.setText("Pet 1");
		player2Pet1Name.setBounds(194, 322, 73, 22);
		player2Pet1Name.setColumns(10);

		player2Pet2Name = new JTextField();
		player2Pet2Name.setText("Pet 2");
		player2Pet2Name.setBounds(282, 322, 73, 22);
		player2Pet2Name.setColumns(10);

		player2Pet3Name = new JTextField();
		player2Pet3Name.setText("Pet 3");
		player2Pet3Name.setBounds(365, 322, 73, 22);
		player2Pet3Name.setColumns(10);

		player3Pet1Name = new JTextField();
		player3Pet1Name.setText("Pet 1");
		player3Pet1Name.setBounds(194, 355, 73, 22);
		player3Pet1Name.setColumns(10);

		player3Pet2Name = new JTextField();
		player3Pet2Name.setText("Pet 2");
		player3Pet2Name.setBounds(282, 355, 73, 22);
		player3Pet2Name.setColumns(10);

		player3Pet3Name = new JTextField();
		player3Pet3Name.setText("Pet 3");
		player3Pet3Name.setBounds(365, 355, 73, 22);
		player3Pet3Name.setColumns(10);

		player1Pet1Species = new JComboBox();
		player1Pet1Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player1Pet1Species.setBounds(151, 463, 90, 22);

		player1Pet2Species = new JComboBox();
		player1Pet2Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player1Pet2Species.setSelectedIndex(1);
		player1Pet2Species.setBounds(260, 462, 90, 22);

		player1Pet3Species = new JComboBox();
		player1Pet3Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player1Pet3Species.setSelectedIndex(2);
		player1Pet3Species.setBounds(365, 463, 90, 22);

		player2Pet1Species = new JComboBox();
		player2Pet1Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player2Pet1Species.setBounds(151, 495, 91, 22);

		player2Pet2Species = new JComboBox();
		player2Pet2Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player2Pet2Species.setSelectedIndex(1);
		player2Pet2Species.setBounds(260, 495, 90, 22);

		player2Pet3Species = new JComboBox();
		player2Pet3Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player2Pet3Species.setSelectedIndex(2);
		player2Pet3Species.setBounds(365, 495, 90, 22);

		player3Pet1Species = new JComboBox();
		player3Pet1Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player3Pet1Species.setBounds(151, 528, 91, 22);

		player3Pet2Species = new JComboBox();
		player3Pet2Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player3Pet2Species.setSelectedIndex(1);
		player3Pet2Species.setBounds(260, 528, 90, 22);

		player3Pet3Species = new JComboBox<Object>();
		player3Pet3Species.setModel(new DefaultComboBoxModel(new String[] {"Mametachi", "Maskutchi", "Tamatchi"}));
		player3Pet3Species.setSelectedIndex(2);
		player3Pet3Species.setBounds(365, 528, 91, 22);



		comboPlayer1PetNum = new JComboBox();
		comboPlayer1PetNum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Game.player1PetNum = Integer.parseInt(comboPlayer1PetNum.getSelectedItem().toString());
				if(Game.player1PetNum == 1){
					player1Pet2Name.setEnabled(false);
					player1Pet3Name.setEnabled(false);
					player1Pet2Species.setEnabled(false);
					player1Pet3Species.setEnabled(false);

				}
				else if (Game.player1PetNum == 2){
					player1Pet2Name.setEnabled(true);
					player1Pet3Name.setEnabled(false);
					player1Pet2Species.setEnabled(true);
					player1Pet3Species.setEnabled(false);

				}
				else{
					player1Pet2Name.setEnabled(true);
					player1Pet3Name.setEnabled(true);
					player1Pet2Species.setEnabled(true);
					player1Pet3Species.setEnabled(true);
				}

			}
		});
		comboPlayer1PetNum.setBounds(375, 123, 42, 22);
		comboPlayer1PetNum.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3"}));
		comboPlayer1PetNum.setSelectedIndex(2);

		comboPlayer2PetNum = new JComboBox();
		comboPlayer2PetNum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Game.player2PetNum = Integer.parseInt(comboPlayer2PetNum.getSelectedItem().toString());
				if(Game.player2PetNum == 1){
					player2Pet2Name.setEnabled(false);
					player2Pet3Name.setEnabled(false);
					player2Pet2Species.setEnabled(false);
					player2Pet3Species.setEnabled(false);

				}
				else if (Game.player2PetNum == 2){
					player2Pet2Name.setEnabled(true);
					player2Pet3Name.setEnabled(false);
					player2Pet2Species.setEnabled(true);
					player2Pet3Species.setEnabled(false);

				}
				else{
					player2Pet2Name.setEnabled(true);
					player2Pet3Name.setEnabled(true);
					player2Pet2Species.setEnabled(true);
					player2Pet3Species.setEnabled(true);
				}
			}
		});
		comboPlayer2PetNum.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3"}));
		comboPlayer2PetNum.setSelectedIndex(2);
		comboPlayer2PetNum.setBounds(375, 155, 42, 22);

		comboPlayer3PetNum = new JComboBox();
		comboPlayer3PetNum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Game.player3PetNum = Integer.parseInt(comboPlayer3PetNum.getSelectedItem().toString());
				if(Game.player3PetNum == 1){
					player3Pet2Name.setEnabled(false);
					player3Pet3Name.setEnabled(false);
					player3Pet2Species.setEnabled(false);
					player3Pet3Species.setEnabled(false);

				}
				else if (Game.player3PetNum == 2){
					player3Pet2Name.setEnabled(true);
					player3Pet3Name.setEnabled(false);
					player3Pet2Species.setEnabled(true);
					player3Pet3Species.setEnabled(false);

				}
				else{
					player3Pet2Name.setEnabled(true);
					player3Pet3Name.setEnabled(true);
					player3Pet2Species.setEnabled(true);
					player3Pet3Species.setEnabled(true);
				}
			}
		});
		comboPlayer3PetNum.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3"}));
		comboPlayer3PetNum.setSelectedIndex(2);
		comboPlayer3PetNum.setBounds(375, 188, 42, 22);




		final JComboBox comboPlayerNum = new JComboBox();
		comboPlayerNum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Game.numPlayers = Integer.parseInt(comboPlayerNum.getSelectedItem().toString());
				if(Game.numPlayers ==1){
					Game.player2PetNum=0;
					Game.player3PetNum=0;
					player2Name.setText("");
					player3Name.setText(" ");
					player2Name.setEnabled(false);;
					player3Name.setEnabled(false);
					comboPlayer2PetNum.setEnabled(false);
					comboPlayer3PetNum.setEnabled(false);
					player2Pet1Name.setEnabled(false);
					player2Pet2Name.setEnabled(false);
					player2Pet3Name.setEnabled(false);
					player3Pet1Name.setEnabled(false);
					player3Pet2Name.setEnabled(false);
					player3Pet3Name.setEnabled(false);
					player2Pet1Species.setEnabled(false);
					player2Pet2Species.setEnabled(false);
					player2Pet3Species.setEnabled(false);
					player3Pet1Species.setEnabled(false);
					player3Pet2Species.setEnabled(false);
					player3Pet3Species.setEnabled(false);	
				}
				else if(Game.numPlayers ==2){
					Game.player3PetNum=0;
					player3Name.setText("");
					player2Name.setText("Player 2");
					player3Name.setEnabled(false);
					player2Name.setEnabled(true);
					comboPlayer2PetNum.setEnabled(true);
					comboPlayer3PetNum.setEnabled(false);
					player2Pet1Name.setEnabled(true);
					player2Pet2Name.setEnabled(true);
					player2Pet3Name.setEnabled(true);
					player3Pet1Name.setEnabled(false);
					player3Pet2Name.setEnabled(false);
					player3Pet3Name.setEnabled(false);
					player2Pet1Species.setEnabled(true);
					player2Pet2Species.setEnabled(true);
					player2Pet3Species.setEnabled(true);
					player3Pet1Species.setEnabled(false);
					player3Pet2Species.setEnabled(false);
					player3Pet3Species.setEnabled(false);
				}
				else{
					player2Name.setText("Player 2");
					player3Name.setText("Player 3");
					player1Name.setEnabled(true);
					player2Name.setEnabled(true);
					player3Name.setEnabled(true);
					comboPlayer1PetNum.setEnabled(true);
					comboPlayer2PetNum.setEnabled(true);
					comboPlayer3PetNum.setEnabled(true);
					player2Pet1Name.setEnabled(true);
					player2Pet2Name.setEnabled(true);
					player2Pet3Name.setEnabled(true);
					player3Pet1Name.setEnabled(true);
					player3Pet2Name.setEnabled(true);
					player3Pet3Name.setEnabled(true);
					player1Pet1Species.setEnabled(true);
					player1Pet2Species.setEnabled(true);
					player1Pet3Species.setEnabled(true);
					player2Pet1Species.setEnabled(true);
					player2Pet2Species.setEnabled(true);
					player2Pet3Species.setEnabled(true);
					player3Pet1Species.setEnabled(true);
					player3Pet2Species.setEnabled(true);
					player3Pet3Species.setEnabled(true);					
				}
			}
		});


		txtGameSetup = new JTextField();
		txtGameSetup.setBounds(71, 11, 437, 28);
		txtGameSetup.setEditable(false);
		txtGameSetup.setFont(new Font("Tahoma", Font.BOLD, 20));
		txtGameSetup.setHorizontalAlignment(SwingConstants.CENTER);
		txtGameSetup.setText("Game Setup");
		txtGameSetup.setColumns(10);

		enterPetNameText = new JTextField();
		enterPetNameText.setBounds(89, 217, 389, 28);
		enterPetNameText.setText("Please enter a pet name for each pet");
		enterPetNameText.setHorizontalAlignment(SwingConstants.CENTER);
		enterPetNameText.setEditable(false);
		enterPetNameText.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(137, 322, 50, 22);
		textField_1.setText("Player 2");
		textField_1.setEditable(false);
		textField_1.setColumns(10);

		comboPlayerNum.setBounds(370, 50, 47, 22);
		comboPlayerNum.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3"}));
		comboPlayerNum.setSelectedIndex(2);

		textField_2 = new JTextField();
		textField_2.setBounds(137, 289, 50, 22);
		textField_2.setText("Player 1");
		textField_2.setEditable(false);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(137, 355, 50, 22);
		textField_3.setText("Player 3");
		textField_3.setEditable(false);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(194, 256, 73, 22);
		textField_4.setHorizontalAlignment(SwingConstants.CENTER);
		textField_4.setText("Pet 1");
		textField_4.setEditable(false);
		textField_4.setColumns(10);

		txtPet = new JTextField();
		txtPet.setBounds(282, 256, 73, 22);
		txtPet.setText("Pet 2");
		txtPet.setHorizontalAlignment(SwingConstants.CENTER);
		txtPet.setEditable(false);
		txtPet.setColumns(10);



		txtPet_1 = new JTextField();
		txtPet_1.setBounds(365, 256, 73, 22);
		txtPet_1.setText("Pet 3");
		txtPet_1.setHorizontalAlignment(SwingConstants.CENTER);
		txtPet_1.setEditable(false);
		txtPet_1.setColumns(10);



		textField_18 = new JTextField();
		textField_18.setBounds(89, 528, 50, 22);
		textField_18.setText("Player 3");
		textField_18.setEditable(false);
		textField_18.setColumns(10);

		textField_19 = new JTextField();
		textField_19.setBounds(89, 495, 50, 22);
		textField_19.setText("Player 2");
		textField_19.setEditable(false);
		textField_19.setColumns(10);

		textField_26 = new JTextField();
		textField_26.setBounds(89, 462, 50, 22);
		textField_26.setText("Player 1");
		textField_26.setEditable(false);
		textField_26.setColumns(10);

		textField_27 = new JTextField();
		textField_27.setBounds(151, 429, 91, 22);
		textField_27.setText("Pet 1");
		textField_27.setHorizontalAlignment(SwingConstants.CENTER);
		textField_27.setEditable(false);
		textField_27.setColumns(10);

		textField_28 = new JTextField();
		textField_28.setBounds(260, 429, 90, 22);
		textField_28.setText("Pet 2");
		textField_28.setHorizontalAlignment(SwingConstants.CENTER);
		textField_28.setEditable(false);
		textField_28.setColumns(10);

		textField_29 = new JTextField();
		textField_29.setBounds(365, 429, 90, 22);
		textField_29.setText("Pet 3");
		textField_29.setHorizontalAlignment(SwingConstants.CENTER);
		textField_29.setEditable(false);
		textField_29.setColumns(10);

		pleaseEnterSpeciesText = new JTextField();
		pleaseEnterSpeciesText.setBounds(89, 389, 389, 28);
		pleaseEnterSpeciesText.setText("Please enter species for each pet");
		pleaseEnterSpeciesText.setHorizontalAlignment(SwingConstants.CENTER);
		pleaseEnterSpeciesText.setEditable(false);
		pleaseEnterSpeciesText.setColumns(10);



		setupGui.getContentPane().setLayout(null);
		setupGui.getContentPane().add(txtGameSetup);
		setupGui.getContentPane().add(txtNumberOfPlayers);
		setupGui.getContentPane().add(comboPlayerNum);
		setupGui.getContentPane().add(txtPleaseEnterYour);
		setupGui.getContentPane().add(txtPlayer);
		setupGui.getContentPane().add(player1Name);
		setupGui.getContentPane().add(comboPlayer1PetNum);
		setupGui.getContentPane().add(txtPlayer_1);
		setupGui.getContentPane().add(player2Name);
		setupGui.getContentPane().add(comboPlayer2PetNum);
		setupGui.getContentPane().add(txtPlayer_2);
		setupGui.getContentPane().add(player3Name);
		setupGui.getContentPane().add(comboPlayer3PetNum);
		setupGui.getContentPane().add(enterPetNameText);
		setupGui.getContentPane().add(textField_4);
		setupGui.getContentPane().add(txtPet);
		setupGui.getContentPane().add(txtPet_1);
		setupGui.getContentPane().add(textField_2);
		setupGui.getContentPane().add(player1Pet1Name);
		setupGui.getContentPane().add(player1Pet2Name);
		setupGui.getContentPane().add(player1Pet3Name);
		setupGui.getContentPane().add(textField_1);
		setupGui.getContentPane().add(player2Pet1Name);
		setupGui.getContentPane().add(player2Pet2Name);
		setupGui.getContentPane().add(player2Pet3Name);
		setupGui.getContentPane().add(textField_3);
		setupGui.getContentPane().add(player3Pet1Name);
		setupGui.getContentPane().add(player3Pet2Name);
		setupGui.getContentPane().add(player3Pet3Name);
		setupGui.getContentPane().add(pleaseEnterSpeciesText);
		setupGui.getContentPane().add(textField_27);
		setupGui.getContentPane().add(textField_28);
		setupGui.getContentPane().add(textField_29);
		setupGui.getContentPane().add(textField_26);
		setupGui.getContentPane().add(player1Pet1Species);
		setupGui.getContentPane().add(player1Pet2Species);
		setupGui.getContentPane().add(player1Pet3Species);
		setupGui.getContentPane().add(textField_19);
		setupGui.getContentPane().add(player2Pet1Species);
		setupGui.getContentPane().add(player2Pet2Species);
		setupGui.getContentPane().add(player2Pet3Species);
		setupGui.getContentPane().add(textField_18);
		setupGui.getContentPane().add(player3Pet1Species);
		setupGui.getContentPane().add(player3Pet2Species);
		setupGui.getContentPane().add(player3Pet3Species);

		daysToPlayText = new JTextField();
		daysToPlayText.setText("How many days do you want to play?");
		daysToPlayText.setHorizontalAlignment(SwingConstants.CENTER);
		daysToPlayText.setEditable(false);
		daysToPlayText.setColumns(10);
		daysToPlayText.setBounds(161, 562, 266, 26);
		setupGui.getContentPane().add(daysToPlayText);

		daysToPlay = new JTextField();
		daysToPlay.setFont(new Font("Dialog", Font.BOLD, 14));
		daysToPlay.setEditable(false);
		daysToPlay.setText("10");
		daysToPlay.setHorizontalAlignment(SwingConstants.CENTER);
		daysToPlay.setColumns(10);
		daysToPlay.setBounds(360, 595, 67, 22);
		setupGui.getContentPane().add(daysToPlay);

		final JSlider daysToPlaySlider = new JSlider();
		daysToPlaySlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				daysToPlay.setText(""+daysToPlaySlider.getValue());
				Game.daysToPlay = daysToPlaySlider.getValue();
			}
		});
		daysToPlaySlider.setMinimum(1);
		daysToPlaySlider.setMaximum(50);
		daysToPlaySlider.setValue(10);
		daysToPlaySlider.setBounds(151, 595, 187, 16);
		setupGui.getContentPane().add(daysToPlaySlider);



		button = new JButton("Done");
		button.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				//Passing Information to Game class	
				Game.player1Name = player1Name.getText(); 			
				Game.player2Name = player2Name.getText(); 
				Game.player3Name =	player3Name.getText(); 
				if (checkPlayerNames(Game.player1Name,Game.player2Name,Game.player3Name)== true){
					JOptionPane.showMessageDialog(setupGui, "You have duplicate player names!");
				}
				else if (checkPetNames(player1Pet1Name, player1Pet2Name, player1Pet3Name, player2Pet1Name, player2Pet2Name, 
						player2Pet3Name, player3Pet1Name, player3Pet2Name, player3Pet3Name) == true){
					JOptionPane.showMessageDialog(setupGui, "You have duplicate pet names for a player!");
				}
				else{
					// Player 1
					Game.petNameList1.add(player1Pet1Name.getText());
					Game.speciesList1.add(player1Pet1Species.getSelectedItem().toString());
					if (Game.player1PetNum >1)	{
						Game.petNameList1.add(player1Pet2Name.getText());
						Game.speciesList1.add(player1Pet2Species.getSelectedItem().toString());
					}
					if (Game.player1PetNum >2)	{
						Game.petNameList1.add(player1Pet3Name.getText());
						Game.speciesList1.add(player1Pet3Species.getSelectedItem().toString());
					}		
					// Player 2
					if (Game.player2PetNum >0)	{
						Game.petNameList2.add(player2Pet1Name.getText());
						Game.speciesList2.add(player2Pet1Species.getSelectedItem().toString());
					}
					if (Game.player2PetNum >1)	{
						Game.petNameList2.add(player2Pet2Name.getText());
						Game.speciesList2.add(player2Pet2Species.getSelectedItem().toString());
					}
					if (Game.player2PetNum >2)	{
						Game.petNameList2.add(player2Pet3Name.getText());
						Game.speciesList2.add(player2Pet3Species.getSelectedItem().toString());
					}
					//Player 3
					if (Game.player3PetNum >0)	{
						Game.petNameList3.add(player3Pet1Name.getText());
						Game.speciesList3.add(player3Pet1Species.getSelectedItem().toString());
					}
					if (Game.player3PetNum >1)	{
						Game.petNameList3.add(player3Pet2Name.getText());
						Game.speciesList3.add(player3Pet2Species.getSelectedItem().toString());
					}
					if (Game.player3PetNum >2)	{
						Game.petNameList3.add(player3Pet3Name.getText());
						Game.speciesList3.add(player3Pet3Species.getSelectedItem().toString());
					}
					Game.daysToPlay = daysToPlaySlider.getValue();
					//Build the pets required for the game
					Game.buildPets();
					// Run next GUI
					setupGui.dispose();
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								Gui window = new Gui();
								window.frame.setVisible(true);
							}
							catch (Exception e) {
								System.out.println("ERROR!");
								e.printStackTrace();
							}
						}
					});
				}
			}
		});	
		button.setBounds(223, 623, 120, 36);
		setupGui.getContentPane().add(button);		
	}
}
