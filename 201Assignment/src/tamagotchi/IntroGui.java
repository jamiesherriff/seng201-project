package tamagotchi;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

	/**
	 * Intro Gui that tells the user what to do
	 *Can launch or quit the game
	 * @author Jamie
	 *
	 */
public class IntroGui {

	JFrame frame;
	private JTextField txtTheInteractivePet;
	private JTextField txtWelcome;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					IntroGui window = new IntroGui();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	 public IntroGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the IntroGui frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setMinimumSize(new Dimension(800, 500));
		frame.setBounds(500, 300, 800, 702);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(99, 27, 600, 186);
		lblNewLabel.setIcon(new ImageIcon(IntroGui.class.getResource("/Pictures/Tamagotchi.png")));
		
		JButton btnNewButton = new JButton("New Game");
		btnNewButton.setBounds(99, 559, 212, 80);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							SetupGui window = new SetupGui();
							window.setupGui.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		
		JButton btnExit = new JButton("Exit");
		btnExit.setBounds(487, 559, 212, 80);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		txtWelcome = new JTextField();
		txtWelcome.setEditable(false);
		txtWelcome.setBounds(330, 516, 136, 49);
		txtWelcome.setFont(new Font("Tahoma", Font.PLAIN, 30));
		txtWelcome.setText("Welcome");
		txtWelcome.setColumns(10);
		
		txtTheInteractivePet = new JTextField();
		txtTheInteractivePet.setBounds(99, 219, 600, 43);
		txtTheInteractivePet.setEditable(false);
		txtTheInteractivePet.setHorizontalAlignment(SwingConstants.CENTER);
		txtTheInteractivePet.setText("The Interactive Pet Simulator Game");
		txtTheInteractivePet.setFont(new Font("Tahoma", Font.PLAIN, 30));
		txtTheInteractivePet.setColumns(10);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(txtWelcome);
		frame.getContentPane().add(txtTheInteractivePet);
		frame.getContentPane().add(btnNewButton);
		frame.getContentPane().add(btnExit);
		frame.getContentPane().add(lblNewLabel);
		
		JTextArea txtrThisIsA = new JTextArea();
		txtrThisIsA.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		txtrThisIsA.setEditable(false);
		txtrThisIsA.setText("The idea of Tamagotchi is to look after a \u201Cdigital\u201D pet as you would for a real pet. You can\r\nfeed it, interact with it and put it to sleep. Pets can also get jealous of other pets.\r\nThe goal of the game is to get a higher than other players by taking better care of your pets.\r\nThe more pets you have the higher the potential score but more care is required.\r\n\r\nBut be careful as pets can die and can only be revived once and a pently occurs to the players score.\r\n\r\nThe game is progressed by ending the current players turn and then ending the current day.\r\nAt the end of the game the winning player is shown\r\n\r\nMinimum screen resoultion required: 1024x768");
		txtrThisIsA.setBounds(109, 274, 591, 209);
		frame.getContentPane().add(txtrThisIsA);
	}
}
