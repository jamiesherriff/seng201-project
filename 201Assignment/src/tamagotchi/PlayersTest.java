package tamagotchi;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PlayersTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testCalculateWinner() {
		String value;
		String value2;
		String value3;
		String value4;
		int []player1 = {0,500,1,0};
		int []player2 ={0,500,1,0};
		int []player3 ={0,500,1,0};
		int []player4 ={0,500436364,1,0};
		int []player5 ={0,0,1,0};
		int []player6 ={0,-34252562,1,0};
		String winner = "Player 1";
		String winner2 = "Player 2";
		String winner3 = "Player 3";
		String draw = "Everyone! there was a draw";
		value = Players.calculateWinner(player1,player2,player3);
		assertEquals(value,draw);
		value2 = Players.calculateWinner(player4,player5,player6);
		assertEquals(value2,winner);
		value3 = Players.calculateWinner(player6,player4,player6);
		assertEquals(value3,winner2);
		value4 = Players.calculateWinner(player5,player6,player4);
		assertEquals(value4,winner3);
		
	}

}
