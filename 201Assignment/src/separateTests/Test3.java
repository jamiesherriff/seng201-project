package separateTests;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

import tamagotchi.Game;











public class Test3 {

	/**
	 * @param args
	 */
	final Object lock = new Object();
	static int numPlayers = 3;
	 static int daysToPlay =5;
	 //static String[] nameList = {"player1","player2","player3"}; TODO
	 static String player1Name ="";
	 static String player2Name = "";
	 static String player3Name = "";
	 static int player1PetNum;
	 static int player2PetNum;
	 static int player3PetNum;
	 //static int[] petNumberList ={1,0,0}; TODO
	 static String[] player1pets;
	 static List<String> petNameList1 = new ArrayList<String>();
	 static List<String> speciesList1 = new ArrayList<String>();
	 static List<String> petNameList2 = new ArrayList<String>();
	 static List<String> speciesList2 = new ArrayList<String>();
	 static List<String> petNameList3 = new ArrayList<String>();
	 static List<String> speciesList3 = new ArrayList<String>();
	 static List<int[]> petList = new ArrayList<int[]>();
	 static List<int[]> petList1 = new ArrayList<int[]>();
	 static int daycount=0;
	 static int playerActions= 2;
	 static boolean endTurn = false;
	 boolean gamerunning=true;
	 static boolean endDay = false;
	 static int playerslefttogo = numPlayers;
	 static String weather;
	 static String winningPlayer="";
	 static boolean startGame=false;
		static int[] currentPet;
		static int[] currentPlayer;
		
	 
	  static int[] tamatchiPet = {40,80,30,30,8,2,0,0};
	  static int[] maskutchiPet = {50,40,70,50,4,2,0,0};
	  static int[] mametachiPet = {50,50,40,60,6,2,0,0};
	  static final int[] emptyPet = {};
	  static int[][] petList2 = new int[7][9];
	  
		static int[] player1 = {20*player1PetNum*daysToPlay,0,1};
		static int[] player2 = {20*player2PetNum*daysToPlay,0,2};
		static int[] player3 = {20*player3PetNum*daysToPlay,0,3};
		 static int[][] petMatrix = new int[0][];
	 
	 static void buildPets()
	 				//player 1
		{		for (int i = 0; i < speciesList1.size(); i++) {
			for (int j = 0; j < speciesList1.size(); j++) {
						if (speciesList1.get(i)== "Mametachi"){
							System.out.println(i);
							petList2[j]= mametachiPet;
						}
						else if (speciesList1.get(i)== "Tamatchi")	{
							petList2[j]=tamatchiPet;
						}
						else if (speciesList1.get(i)== "Maskutchi")	{
							petList2[j]=maskutchiPet;
						}
			}
					}

		}

	 static void feedpet(int[] pet, int[] player, String food){
			if (pet == null || currentPlayer == null){
				System.out.println(pet);
				System.out.println(food);
				JOptionPane.showMessageDialog(null, "Please Select a Pet");
			}

			else if (pet[5]<=0){
				JOptionPane.showMessageDialog(null, "No actions left for this pet");
				return;
			}
			else if(pet[1] >= 100){
				JOptionPane.showMessageDialog(null, "This pet can not eat anymore");
			}
			else if(pet[0] >= 100){
				JOptionPane.showMessageDialog(null, "This pet can not get any healthier!");	
			}

			else{
				if(food == "Dinner - Costs $25")
				{
					System.out.println("Here"+ pet[0]);
					pet[0] = pet[0]+10;
					pet[1] = pet[1]+20;
					pet[5] = pet[5]-1;
					pet[4] = pet[4] +2;
					player[0]= player[0] -25;
				}
				if (food == "Snack - Costs $10")
				{
					pet[0] = pet[0]+2;
					pet[1]= pet[1]+5;
					pet[5] = pet[5]-1;
					player[0]= player[0] -10;
					pet[4] = pet[4] +1;
				}
				if (food == "Medicine - Costs $50"){
					pet[6] = 0;
					pet[0] = pet[0]+10;
					player[0]= player[0] -50;
				}
				if (pet[0]>100){ //adjusting for case if health exceeds 100
					pet[0] = 100;

				}
				if (pet[1] >100){ //adjusting for case if hunger exceeds 100
					pet[1] =100;
				}
			}
			currentPet = pet;
			currentPlayer = player;

		}
	
	 static void buildPetsInital(){
		 for (int i = 0; i < 9; i++) {
					petList1.add(emptyPet);
				}
		 }
	 
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		petNameList1.add("Roofus");
		petNameList1.add("Chamelon");
		petNameList1.add("Roofus");
		petNameList2.add("Fire");
		petNameList2.add("Water");
		petNameList2.add("Grass");
		petNameList3.add("Barky");
				
		speciesList1.add("Mametachi");
		speciesList1.add( "Tamatchi");
		speciesList1.add ("Maskutchi");
		
		speciesList2.add ("Tamatchi");
		speciesList2.add ("Tamatchi");
		speciesList2.add ("Tamatchi");
		speciesList3.add( "Maskutchi");
		//System.out.println((Arrays.deepToString(petList.toArray())));
		//currentPet = petList.get(1);
		Arrays.toString(currentPet);
		buildPetsInital();
		
		//System.out.println(currentPet[0]);
		//System.out.println(Arrays.toString(currentPet));
		//feedpet(currentPet, player1, "Dinner - Costs $25");
		System.out.println((Arrays.deepToString(petList1.toArray())));
		System.out.println((Arrays.deepToString(petList2)));
		buildPets();
		System.out.println((Arrays.deepToString(petList1.toArray())));
		System.out.println((Arrays.deepToString(petList2)));
		//petList1.set(1, maskutchiPet);
		//System.out.println((Arrays.deepToString(petList1.toArray())));
		
		petMatrix =petList.toArray(new int[0][]);
		System.out.println(Arrays.deepToString(petMatrix));
		
	}

}
